@echo off
mkdir Old_Rundowns > nul 2>&1
set /p SteamUsername=<_Steam_Username.txt
if "%SteamUsername%" == "changeme" (
	msg "%username%" "Please enter the username of your Steam account in the _Steam_Username.txt file!"
) else (
	echo Launching DepotDownloader - please be aware you'll need to log in to your Steam account, and your password WILL NOT be saved.
	echo If your account uses Steam Guard, you'll have to get the login code and enter it for DepotDownloader!
	DepotDownloader.exe -app 493520 -depot 493521 -manifest 8000105074755596729 -dir .\Old_Rundowns\Rundown_001 -username "%SteamUsername%"
	pause
)
