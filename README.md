# <u>PLEASE DO NOT SHARE THIS REPO</u>

If you've received a link to this repository, it's (hopefully) because you're looking to play the old rundowns with a small group of players that are also interested. The playing of old rundowns is discouraged by both the official and modding discords for GTFO, and to respect that I am trying to keep this repository as hidden as possible.

Please do not share the repository around. If you don't trust the code, and you want to have a friend look at it to ensure nothing fishy is being done, then please do so - but try and keep the exposure of the repository to a minimum. You're free to compile it yourself as well if you don't trust me, or have a friend you trust do it!

## What this mod does

The short and simple is that it:

- Creates a new folder for saving your config files for Rundown 2 - **your current GTFO settings <u>will not</u> be changed** because of this.
- Removes any (HTTP) calls to the official GTFO servers - **this <u>will not</u> affect your current GTFO progression in any way, and <u>will not</u> make any (identifiable or not) requests to GTFO's servers**. You <u>**do not**</u> have to worry about any bans for playing the old version of the game.

Currently it's built to work with the Goldberg Steam Emulator, which is great to use when you're looking to play Steam games on LAN when you don't have an Internet connection. More information and downloads for the project can be found at the project's GitLab page https://gitlab.com/Mr_Goldberg/goldberg_emulator.

## Downloading old versions of GTFO

Since the official Steam client doesn't support this anymore, we'll need to use a tool capable of doing so. The files that you are downloading are **from the official Steam servers**, and this **is not** something that will get you banned from Steam for doing, so don't worry.

The tool we'll be using is Depot Downloader, a program that has been around for several years. It can be found at its GitHub repository https://github.com/SteamRE/DepotDownloader and you can also find the downloads for it there (or for its latest download, here https://github.com/SteamRE/DepotDownloader/releases/latest).

(I'll eventually put a better description for it here, but here's a quick guide on how to download Rundown 2)

Option 1: Use the script in the scripts folder named "_Download Rundown 2.bat". You'll also need to change _Steam_Username.txt to the username that you log into Steam with.
Option 2: Open command prompt and run the command manually. cd into the directory you downloaded and extracted DepotDownloader into, and run the following `DepotDownloader.exe -app 493520 -depot 493521 -manifest 2523183852652622954 -username ?????` (and make sure to change the ????? to your Steam username)

