﻿using System.Collections.Generic;
using HarmonyLib;
using PlayFab;
using UnityEngine;

namespace GTFORundown2OfflinePatch.Patches
{
    [HarmonyPatch(typeof(PlayFabSettings), nameof(PlayFabSettings.GetFullUrl))]
    class GetFullUrlPatch
    {
        static void Postfix(string apiCall,
            Dictionary<string, string> getParams,
            PlayFabApiSettings apiSettings,
            string __result)
        {
            var domainStart = __result.IndexOf("://") + 3;
            if (domainStart == -1)
            {
                goto MalformedUrl;
            }

            var domainEnd = __result.IndexOf('/', domainStart);
            if (domainEnd == -1)
            {
                goto MalformedUrl;
            }

            var fixedUrl = $"http://rundown2.playfabemu.local:7504{__result.Substring(domainEnd)}";
            Debug.LogWarning($"Redirecting PlayFab request from url {__result} to {fixedUrl}");
            __result = fixedUrl;
            return;

        MalformedUrl:
            Debug.LogError($"Malformed PlayFab url {__result}");
            return;
        }
    }
}
