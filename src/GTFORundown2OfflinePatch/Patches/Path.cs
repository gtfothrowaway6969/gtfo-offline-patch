﻿using System;
using System.IO;
using HarmonyLib;
using UnityEngine;

namespace GTFORundown2OfflinePatch.Patches
{
    [HarmonyPatch(typeof(Path), nameof(Path.Combine), typeof(string), typeof(string))]
    class PathCombinePatch
    {
        static bool Prefix(ref string path1, string path2)
        {
            if (path1.Equals(Application.persistentDataPath, StringComparison.OrdinalIgnoreCase))
                path1 = GTFORundown2OfflinePlugin.Instance.NewGTFODirectory;

            return true;
        }
    }
}
