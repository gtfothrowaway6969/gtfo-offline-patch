﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using HarmonyLib;
using Steamworks;
using UnityEngine;

namespace GTFORundown2OfflinePatch.Patches
{
    [HarmonyPatch(typeof(PlayFabManager), "LoginWithSteam")]
    class LoginWithSteamPatch
    {
        static bool Prefix(
            PlayFabManager __instance, 
            ref bool ___m_loggedIn,
            ref string ___m_entityId, 
            ref string ___m_entityType)
        {
            var loadAllFiles = AccessTools.Method(__instance.GetType(), "LoadAllFiles");
            var onLocalPlayerDataLoaded = AccessTools.Method(__instance.GetType(), "OnLocalPlayerDataLoaded");
            var onTitleDataLoaded = AccessTools.Method(__instance.GetType(), "OnTitleDataLoaded");

            if (loadAllFiles == null)
            {
                Debug.LogError($"{nameof(LoginWithSteamPatch)} can't execute - couldn't find {nameof(loadAllFiles)} method.");
                return true;
            }

            if (onLocalPlayerDataLoaded == null)
            {
                Debug.LogError($"{nameof(LoginWithSteamPatch)} can't execute - couldn't find {nameof(onLocalPlayerDataLoaded)} method.");
                return true;
            }

            if (onTitleDataLoaded == null)
            {
                Debug.LogError($"{nameof(LoginWithSteamPatch)} can't execute - couldn't find {nameof(onTitleDataLoaded)} method.");
                return true;
            }

            Debug.Log("Faking PlayFab login result...");

            ___m_loggedIn = true;
            ___m_entityId = "StubEntityId";
            ___m_entityType = "StubEntityType";
            PlayFabManager.PlayFabId = "StubPlayFabId";
            PlayFabManager.LoggedInDateTime = DateTime.Now;
            PlayFabManager.LoggedInSeconds = 1f;
            loadAllFiles.Invoke(__instance, null);
            onLocalPlayerDataLoaded.Invoke(__instance, null);
            onTitleDataLoaded.Invoke(__instance, null);

            Debug.Log("Done!");
            return false;
        }
    }

    [HarmonyPatch(typeof(PlayFabManager), "LoadAllFiles")]
    class LoadAllFilesPatch
    {
        static bool Prefix(
            PlayFabManager __instance, 
            Dictionary<string, string> ___m_playerEntityFiles)
        {
            var setupPlayerRundownProgressionFile = AccessTools.Method(typeof(RundownManager), "SetupPlayerRundownProgressionFile");
            if (setupPlayerRundownProgressionFile == null)
            {
                Debug.LogError($"{nameof(LoadAllFilesPatch)} can't execute - couldn't find {nameof(setupPlayerRundownProgressionFile)} method.");
                return true;
            }

            Debug.Log("Redirecting LoadAllFiles to load files from disk...");

            var remoteStorageFiles = Directory.GetFiles(GTFORundown2OfflinePlugin.Instance.RemoteStoragePath);

            foreach (var path in remoteStorageFiles)
            {
                var fileName = Path.GetFileName(path);
                var contents = File.ReadAllText(path, Encoding.UTF8);
                Debug.Log($"Loading player entity file {fileName}:");
                Debug.Log(contents);
                ___m_playerEntityFiles.Add(fileName, contents);
            }

            PlayFabManager.PlayerEntityFilesLoaded = true;
            setupPlayerRundownProgressionFile.Invoke(RundownManager.Current, null);
            Debug.Log("Done!");
            return false;
        }
    }

    [HarmonyPatch(typeof(PlayFabManager), "TryGetStartupScreenData")]
    class TryGetStartupScreenDataPatch
    {
        static bool Prefix(PlayFabManager __instance,
            eStartupScreenKey key, 
            out StartupScreenData data,
            ref bool __result)
        {
            //Debug.Log("Spoofing Rundown startup screen data...");
            // TODO: is this done correctly?

            data = new StartupScreenData
            {
                AllowedToStartGame = true,
                IntroText = "GTFO Rundown 2 - Offline Patch",
                ShowBugReportButton = false,
                ShowDiscordButton = false,
                ShowIntroText = true
            };
            __result = true;

            //Debug.Log("Done!");
            return false;
        }
    }

    // NOTE: It's Titel, not Title
    [HarmonyPatch(typeof(PlayFabManager), "RefreshStartupScreenTitelData")]
    class RefreshStartupScreenTitelDataPatch
    {
        static bool Prefix(eStartupScreenKey key, Action OnSuccess)
        {
            OnSuccess?.Invoke();
            return false;
        }
    }


    [HarmonyPatch(typeof(PlayFabManager), nameof(PlayFabManager.UploadPlayerEntityFile))]
    class UploadPlayerEntityFilePatch
    {
        static bool Prefix(string fileName)
        {
            Debug.Log($"PlayFabManager.UploadPlayerEntityFile {fileName} PlayerEntityFilesLoaded: {PlayFabManager.PlayerEntityFilesLoaded}");

            if (!PlayFabManager.PlayerEntityFilesLoaded)
            {
                Debug.LogError($"PlayFabManager.UploadPlayerEntityFile, trying to upload an entity file ({fileName}) although the files are not initially loaded or setup");
                return false;
            }

            if (!PlayFabManager.TryGetPlayerEntityFileValue(fileName, out var s))
            {
                Debug.LogError($"playerEntityFile {fileName} doesn't have an entry!");
                return false;
            }

            var outPath = Path.Combine(GTFORundown2OfflinePlugin.Instance.RemoteStoragePath, fileName);
            File.WriteAllText(outPath, s, Encoding.UTF8);
            return false;
        }
    }

    // WARNING: This breaks support for Rundown 1
    [HarmonyPatch(typeof(PlayFabManager), nameof(PlayFabManager.TryGetRundownTimerData))]
    class TryGetRundownTimerDataPatch
    {
        static bool Prefix(out RundownTimerData data, ref bool __result)
        {
            data = new RundownTimerData
            {
                ShowCountdownTimer = false,
                ShowScrambledTimer = false
            };
            __result = true;
            return false;
        }
    }
}
