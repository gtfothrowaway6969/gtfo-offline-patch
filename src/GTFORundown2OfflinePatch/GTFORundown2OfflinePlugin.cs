﻿using System.IO;
using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace GTFORundown2OfflinePatch
{
    [BepInPlugin("anon.gtforundown2offlineplugin", "GTFO Rundown 2 Offline Plugin", "1.0.0.0")]
    [BepInProcess("GTFO.exe")]
    public class GTFORundown2OfflinePlugin : BaseUnityPlugin
    {
        public static GTFORundown2OfflinePlugin Instance { get; private set; }

        private readonly Harmony _harmony = new Harmony("anon.gtforundown2offlineplugin");

        private ConfigEntry<string> cfgNewConfigFolderName;

        public string NewGTFODirectory => Path.Combine(
            Path.GetDirectoryName(Application.persistentDataPath), 
            cfgNewConfigFolderName.Value //"GTFO_Rundown2_Offline"
        );

        public string RemoteStoragePath => Path.Combine(NewGTFODirectory, "RemoteStorage");

        public GTFORundown2OfflinePlugin() => Instance = this;

        private void Awake()
        {
            _harmony.PatchAll();

            cfgNewConfigFolderName = Config.Bind("General", "NewConfigFolderName", "GTFO_Rundown2_Offline");

            if (!Directory.Exists(NewGTFODirectory))
            {
                Debug.Log($"Creating new GTFO directory for the patch ({NewGTFODirectory})...");
                Directory.CreateDirectory(NewGTFODirectory);
            }

            if (!Directory.Exists(RemoteStoragePath))
            {
                Debug.Log($"Creating subfolder for storing game save files... ({RemoteStoragePath})...");
                Directory.CreateDirectory(RemoteStoragePath);
            }

            Debug.Log($"{nameof(Application.persistentDataPath)}: {Application.persistentDataPath}");
            Globals.Global.SkipIntro = true;
        }
    }
}
